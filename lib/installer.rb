require 'cocoapods'
module Pod 
  class Installer
    def self.targets_from_sandbox(sandbox, podfile, lockfile)
      raise Informative, 'You must run `pod install` to be able to generate target information' unless lockfile

      new(sandbox, podfile, lockfile).instance_exec do
        plugin_sources = run_source_provider_hooks
        analyzer = create_analyzer(plugin_sources)
        analyze(analyzer)
        if analysis_result.needs_install?
            # TODO run `pod install`
            raise "Run pod install"
        end
        aggregate_targets
      end
    end
    class Analyzer
      class AnalysisResult
        def needs_install?
          podfile_touched = podfile_state.added | podfile_state.changed | podfile_state.deleted
          sandbox_touched = sandbox_state.added | sandbox_state.changed | sandbox_state.deleted
          !podfile_touched.empty? || !sandbox_touched.empty?
        end
      end
    end
  end
end

