with (import <nixpkgs> {});
let
  env = bundlerEnv {
    name = "cocoapods-nix-example-bundler-env";
    inherit ruby;
    gemfile  = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset   = ./gemset.nix;
  };
in stdenv.mkDerivation {
  name = "cocoapods-nix-example";
  buildInputs = [ env env.wrappedRuby ];
}
