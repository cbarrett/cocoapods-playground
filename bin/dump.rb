#! /usr/bin/env ruby
require 'cocoapods-core'

spec = Pod::Specification.from_file("data/Example1.podspec.json")
puts spec.source
puts spec.consumer(:ios).source_files

podfile = Pod::Podfile.from_file("data/Example2.podfile")
puts podfile.root_target_definitions.map { |x| x.to_hash }

podfile = Pod::Podfile.from_file("data/Example3.podfile")
puts podfile.root_target_definitions[0].children[0].dependencies[0].inspect
