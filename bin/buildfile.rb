#! /usr/bin/env ruby
require 'erb'
require 'yaml'
require 'cocoapods'
require_relative '../lib/installer.rb'

sandbox_path = Pathname.new("data/Example4/Pods")
podfile_path = Pathname.new("data/Example4/Podfile")
lockfile_path = Pathname.new("data/Example4/Podfile.lock")
sandbox = Pod::Sandbox.new(sandbox_path) 
podfile = Pod::Podfile.from_file(podfile_path) 
lockfile = Pod::Lockfile.from_file(lockfile_path)

targets = Pod::Installer.targets_from_sandbox(sandbox, podfile, lockfile) 
puts targets.map(&:inspect)
pods = targets.flat_map(&:pod_targets)
puts pods.map(&:inspect)

out1 = File.read("data/Example4/build.swift-build.erb")
out2 = ERB.new(out1, nil, '%').result
puts out2
out3 = YAML.load(out2)
puts out3

